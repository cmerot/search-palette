var searchPalette = (function (exports, React, Emotion, Elasticlunr) {
  'use strict';

  var out_of_memory = /* tuple */[
    "Out_of_memory",
    0
  ];

  var sys_error = /* tuple */[
    "Sys_error",
    -1
  ];

  var failure = /* tuple */[
    "Failure",
    -2
  ];

  var invalid_argument = /* tuple */[
    "Invalid_argument",
    -3
  ];

  var end_of_file = /* tuple */[
    "End_of_file",
    -4
  ];

  var division_by_zero = /* tuple */[
    "Division_by_zero",
    -5
  ];

  var not_found = /* tuple */[
    "Not_found",
    -6
  ];

  var match_failure = /* tuple */[
    "Match_failure",
    -7
  ];

  var stack_overflow = /* tuple */[
    "Stack_overflow",
    -8
  ];

  var sys_blocked_io = /* tuple */[
    "Sys_blocked_io",
    -9
  ];

  var assert_failure = /* tuple */[
    "Assert_failure",
    -10
  ];

  var undefined_recursive_module = /* tuple */[
    "Undefined_recursive_module",
    -11
  ];

  out_of_memory.tag = 248;

  sys_error.tag = 248;

  failure.tag = 248;

  invalid_argument.tag = 248;

  end_of_file.tag = 248;

  division_by_zero.tag = 248;

  not_found.tag = 248;

  match_failure.tag = 248;

  stack_overflow.tag = 248;

  sys_blocked_io.tag = 248;

  assert_failure.tag = 248;

  undefined_recursive_module.tag = 248;
  /*  Not a pure module */

  function caml_array_sub(x, offset, len) {
    var result = new Array(len);
    var j = 0;
    var i = offset;
    while(j < len) {
      result[j] = x[i];
      j = j + 1 | 0;
      i = i + 1 | 0;
    }  return result;
  }
  /* No side effect */

  function app(_f, _args) {
    while(true) {
      var args = _args;
      var f = _f;
      var init_arity = f.length;
      var arity = init_arity === 0 ? 1 : init_arity;
      var len = args.length;
      var d = arity - len | 0;
      if (d === 0) {
        return f.apply(null, args);
      } else if (d < 0) {
        _args = caml_array_sub(args, arity, -d | 0);
        _f = f.apply(null, caml_array_sub(args, 0, arity));
        continue ;
      } else {
        return (function(f,args){
        return function (x) {
          return app(f, args.concat(/* array */[x]));
        }
        }(f,args));
      }
    }}

  function curry_1(o, a0, arity) {
    switch (arity) {
      case 1 : 
          return o(a0);
      case 2 : 
          return (function (param) {
              return o(a0, param);
            });
      case 3 : 
          return (function (param, param$1) {
              return o(a0, param, param$1);
            });
      case 4 : 
          return (function (param, param$1, param$2) {
              return o(a0, param, param$1, param$2);
            });
      case 5 : 
          return (function (param, param$1, param$2, param$3) {
              return o(a0, param, param$1, param$2, param$3);
            });
      case 6 : 
          return (function (param, param$1, param$2, param$3, param$4) {
              return o(a0, param, param$1, param$2, param$3, param$4);
            });
      case 7 : 
          return (function (param, param$1, param$2, param$3, param$4, param$5) {
              return o(a0, param, param$1, param$2, param$3, param$4, param$5);
            });
      default:
        return app(o, /* array */[a0]);
    }
  }

  function _1(o, a0) {
    var arity = o.length;
    if (arity === 1) {
      return o(a0);
    } else {
      return curry_1(o, a0, arity);
    }
  }

  function __1(o) {
    var arity = o.length;
    if (arity === 1) {
      return o;
    } else {
      return (function (a0) {
          return _1(o, a0);
        });
    }
  }

  function curry_2(o, a0, a1, arity) {
    switch (arity) {
      case 1 : 
          return app(o(a0), /* array */[a1]);
      case 2 : 
          return o(a0, a1);
      case 3 : 
          return (function (param) {
              return o(a0, a1, param);
            });
      case 4 : 
          return (function (param, param$1) {
              return o(a0, a1, param, param$1);
            });
      case 5 : 
          return (function (param, param$1, param$2) {
              return o(a0, a1, param, param$1, param$2);
            });
      case 6 : 
          return (function (param, param$1, param$2, param$3) {
              return o(a0, a1, param, param$1, param$2, param$3);
            });
      case 7 : 
          return (function (param, param$1, param$2, param$3, param$4) {
              return o(a0, a1, param, param$1, param$2, param$3, param$4);
            });
      default:
        return app(o, /* array */[
                    a0,
                    a1
                  ]);
    }
  }

  function _2(o, a0, a1) {
    var arity = o.length;
    if (arity === 2) {
      return o(a0, a1);
    } else {
      return curry_2(o, a0, a1, arity);
    }
  }

  function __2(o) {
    var arity = o.length;
    if (arity === 2) {
      return o;
    } else {
      return (function (a0, a1) {
          return _2(o, a0, a1);
        });
    }
  }
  /* No side effect */

  var undefinedHeader = /* array */[];

  function some(x) {
    if (x === undefined) {
      var block = /* tuple */[
        undefinedHeader,
        0
      ];
      block.tag = 256;
      return block;
    } else if (x !== null && x[0] === undefinedHeader) {
      var nid = x[1] + 1 | 0;
      var block$1 = /* tuple */[
        undefinedHeader,
        nid
      ];
      block$1.tag = 256;
      return block$1;
    } else {
      return x;
    }
  }

  function nullable_to_opt(x) {
    if (x === null || x === undefined) {
      return undefined;
    } else {
      return some(x);
    }
  }

  function valFromOption(x) {
    if (x !== null && x[0] === undefinedHeader) {
      var depth = x[1];
      if (depth === 0) {
        return undefined;
      } else {
        return /* tuple */[
                undefinedHeader,
                depth - 1 | 0
              ];
      }
    } else {
      return x;
    }
  }
  /* No side effect */

  function get(arr, i) {
    if (i >= 0 && i < arr.length) {
      return some(arr[i]);
    }
    
  }

  function getExn(arr, i) {
    if (!(i >= 0 && i < arr.length)) {
      throw new Error("File \"belt_Array.ml\", line 25, characters 6-12");
    }
    return arr[i];
  }

  function mapU(a, f) {
    var l = a.length;
    var r = new Array(l);
    for(var i = 0 ,i_finish = l - 1 | 0; i <= i_finish; ++i){
      r[i] = f(a[i]);
    }
    return r;
  }

  function map(a, f) {
    return mapU(a, __1(f));
  }

  function mapWithIndexU(a, f) {
    var l = a.length;
    var r = new Array(l);
    for(var i = 0 ,i_finish = l - 1 | 0; i <= i_finish; ++i){
      r[i] = f(i, a[i]);
    }
    return r;
  }

  function mapWithIndex(a, f) {
    return mapWithIndexU(a, __2(f));
  }
  /* No side effect */

  function mapU$1(opt, f) {
    if (opt !== undefined) {
      return some(f(valFromOption(opt)));
    }
    
  }

  function map$1(opt, f) {
    return mapU$1(opt, __1(f));
  }
  /* No side effect */

  // Generated by BUCKLESCRIPT VERSION 5.0.3, PLEASE EDIT WITH CARE

  function getKeyCode(code) {
    if (code !== 13) {
      if (code >= 38) {
        if (code >= 41) {
          return /* Unknown */4;
        } else {
          switch (code - 38 | 0) {
            case 0 : 
                return /* ArrowUp */1;
            case 1 : 
                return /* Unknown */4;
            case 2 : 
                return /* ArrowDown */0;
            
          }
        }
      } else if (code !== 27) {
        return /* Unknown */4;
      } else {
        return /* Escape */3;
      }
    } else {
      return /* Enter */2;
    }
  }

  function scrollIntoView(parentNode, childNode) {
    var parentRect = parentNode.getBoundingClientRect();
    var childRect = childNode.getBoundingClientRect();
    var pt = parentRect.top;
    var ph = parentRect.height;
    var pb = parentRect.bottom;
    var ps = parentNode.scrollTop;
    var ct = childRect.top;
    var cb = childRect.bottom;
    var ch = childRect.height;
    var rt = ct - pt;
    if (rt < 0.0) {
      parentNode.scrollTo(0.0, rt + ps);
      return /* () */0;
    } else if (cb > pb) {
      parentNode.scrollTo(0.0, rt + ch - ph + ps);
      return /* () */0;
    } else {
      return 0;
    }
  }

  function useEventListener(element, eventName, handler) {
    var handlerRef = React.useRef(handler);
    return /* tuple */[
            (function (param) {
                element.addEventListener(eventName, handlerRef.current);
                return /* () */0;
              }),
            (function (param) {
                element.removeEventListener(eventName, handlerRef.current);
                return /* () */0;
              })
          ];
  }

  var nav = (function(url) {
      var metaEl = document.head.querySelector('meta[name~=lunr-base-href]');
      if (metaEl) {
        url = metaEl.getAttribute('value') + url;
      }
      document.location.assign(url);
    });

  var fuzzysearch = (function (needle, haystack) {
      var hlen = haystack.length;
      var nlen = needle.length;
      if (nlen > hlen) {
          return false;
      }
      if (nlen === hlen) {
          return needle === haystack;
      }
      outer: for (var i = 0, j = 0; i < nlen; i++) {
          var nch = needle.charCodeAt(i);
          while (j < hlen) {
          if (haystack.charCodeAt(j++) === nch) {
              continue outer;
          }
          }
          return false;
      }
      return true;
    });
  /* nav Not a pure module */

  function caml_create_bytes(len) {
    if (len < 0) {
      throw [
            invalid_argument,
            "String.create"
          ];
    } else {
      var result = new Array(len);
      for(var i = 0 ,i_finish = len - 1 | 0; i <= i_finish; ++i){
        result[i] = /* "\000" */0;
      }
      return result;
    }
  }

  function caml_blit_bytes(s1, i1, s2, i2, len) {
    if (len > 0) {
      if (s1 === s2) {
        var s1$1 = s1;
        var i1$1 = i1;
        var i2$1 = i2;
        var len$1 = len;
        if (i1$1 < i2$1) {
          var range_a = (s1$1.length - i2$1 | 0) - 1 | 0;
          var range_b = len$1 - 1 | 0;
          var range = range_a > range_b ? range_b : range_a;
          for(var j = range; j >= 0; --j){
            s1$1[i2$1 + j | 0] = s1$1[i1$1 + j | 0];
          }
          return /* () */0;
        } else if (i1$1 > i2$1) {
          var range_a$1 = (s1$1.length - i1$1 | 0) - 1 | 0;
          var range_b$1 = len$1 - 1 | 0;
          var range$1 = range_a$1 > range_b$1 ? range_b$1 : range_a$1;
          for(var k = 0; k <= range$1; ++k){
            s1$1[i2$1 + k | 0] = s1$1[i1$1 + k | 0];
          }
          return /* () */0;
        } else {
          return 0;
        }
      } else {
        var off1 = s1.length - i1 | 0;
        if (len <= off1) {
          for(var i = 0 ,i_finish = len - 1 | 0; i <= i_finish; ++i){
            s2[i2 + i | 0] = s1[i1 + i | 0];
          }
          return /* () */0;
        } else {
          for(var i$1 = 0 ,i_finish$1 = off1 - 1 | 0; i$1 <= i_finish$1; ++i$1){
            s2[i2 + i$1 | 0] = s1[i1 + i$1 | 0];
          }
          for(var i$2 = off1 ,i_finish$2 = len - 1 | 0; i$2 <= i_finish$2; ++i$2){
            s2[i2 + i$2 | 0] = /* "\000" */0;
          }
          return /* () */0;
        }
      }
    } else {
      return 0;
    }
  }

  function bytes_to_string(a) {
    var bytes = a;
    var len = a.length;
    var s = "";
    var s_len = len;
    if ( len <= 4096 && len === bytes.length) {
      return String.fromCharCode.apply(null, bytes);
    } else {
      var offset = 0;
      while(s_len > 0) {
        var next = s_len < 1024 ? s_len : 1024;
        var tmp_bytes = new Array(next);
        caml_blit_bytes(bytes, offset, tmp_bytes, 0, next);
        s = s + String.fromCharCode.apply(null, tmp_bytes);
        s_len = s_len - next | 0;
        offset = offset + next | 0;
      }    return s;
    }
  }

  function bytes_of_string(s) {
    var len = s.length;
    var res = new Array(len);
    for(var i = 0 ,i_finish = len - 1 | 0; i <= i_finish; ++i){
      res[i] = s.charCodeAt(i);
    }
    return res;
  }
  /* No side effect */

  var id = /* record */[/* contents */0];

  function caml_fresh_oo_id(param) {
    id[0] += 1;
    return id[0];
  }

  function create(str) {
    var v_001 = caml_fresh_oo_id();
    var v = /* tuple */[
      str,
      v_001
    ];
    v.tag = 248;
    return v;
  }
  /* No side effect */

  var Exit = create("Pervasives.Exit");
  /* No side effect */

  function map$2(f, param) {
    if (param) {
      var r = _1(f, param[0]);
      return /* :: */[
              r,
              map$2(f, param[1])
            ];
    } else {
      return /* [] */0;
    }
  }
  /* No side effect */

  var $$Error = create("Caml_js_exceptions.Error");
  /* No side effect */

  var Bottom = create("Array.Bottom");
  /* No side effect */

  function sub(s, ofs, len) {
    if (ofs < 0 || len < 0 || ofs > (s.length - len | 0)) {
      throw [
            invalid_argument,
            "String.sub / Bytes.sub"
          ];
    } else {
      var r = caml_create_bytes(len);
      caml_blit_bytes(s, ofs, r, 0, len);
      return r;
    }
  }
  /* No side effect */

  function sub$1(s, ofs, len) {
    return bytes_to_string(sub(bytes_of_string(s), ofs, len));
  }
  /* No side effect */

  function fromList(entries) {
    var dict = { };
    var _param = entries;
    while(true) {
      var param = _param;
      if (param) {
        var match = param[0];
        dict[match[0]] = match[1];
        _param = param[1];
        continue ;
      } else {
        return dict;
      }
    }}
  /* No side effect */

  // Generated by BUCKLESCRIPT VERSION 5.0.3, PLEASE EDIT WITH CARE

  var black = /* `hex */[
    5194459,
    "000000"
  ];

  var green = /* `hex */[
    5194459,
    "008000"
  ];

  var white = /* `hex */[
    5194459,
    "FFFFFF"
  ];
  /* No side effect */

  // Generated by BUCKLESCRIPT VERSION 5.0.3, PLEASE EDIT WITH CARE

  function ruleToJs(rule) {
    var variant = rule[0];
    if (variant >= -659583595) {
      if (variant >= 488687584) {
        if (variant >= 829240095) {
          var match = rule[1];
          return /* tuple */[
                  match[0],
                  fromList(map$2(ruleToJs, match[1]))
                ];
        } else {
          return /* tuple */[
                  "boxShadow",
                  rule[1]
                ];
        }
      } else if (variant >= -434952966) {
        var match$1 = rule[1];
        var value = match$1[1];
        var name = match$1[0];
        if (name === "content") {
          var match$2 = value === "";
          return /* tuple */[
                  name,
                  match$2 ? "\"\"" : value
                ];
        } else {
          return /* tuple */[
                  name,
                  value
                ];
        }
      } else {
        return /* tuple */[
                "transition",
                rule[1]
              ];
      }
    } else if (variant >= -737064851) {
      return /* tuple */[
              "textShadow",
              rule[1]
            ];
    } else {
      return /* tuple */[
              "animation",
              rule[1]
            ];
    }
  }

  function make(rules) {
    return Emotion.css(fromList(map$2(ruleToJs, rules)));
  }

  function string_of_float(f) {
    return "" + (String(f) + "");
  }

  function string_of_color(param) {
    if (typeof param === "number") {
      if (param >= 582626130) {
        return "transparent";
      } else {
        return "currentColor";
      }
    } else {
      var variant = param[0];
      if (variant !== -878128972) {
        if (variant >= 5197569) {
          if (variant >= 5692173) {
            var match = param[1];
            var r = match[0];
            var g = match[1];
            var b = match[2];
            return "rgb(" + (String(r) + (", " + (String(g) + (", " + (String(b) + ")")))));
          } else {
            var match$1 = param[1];
            var h = match$1[0];
            var s = match$1[1];
            var l = match$1[2];
            return "hsl(" + (String(h) + (", " + (String(s) + ("%, " + (String(l) + "%)")))));
          }
        } else if (variant >= 5194459) {
          return "#" + param[1];
        } else {
          var match$2 = param[1];
          var h$1 = match$2[0];
          var s$1 = match$2[1];
          var l$1 = match$2[2];
          var a = match$2[3];
          return "hsla(" + (String(h$1) + (", " + (String(s$1) + ("%, " + (String(l$1) + ("%, " + (string_of_float(a) + ")")))))));
        }
      } else {
        var match$3 = param[1];
        var r$1 = match$3[0];
        var g$1 = match$3[1];
        var b$1 = match$3[2];
        var a$1 = match$3[3];
        return "rgba(" + (String(r$1) + (", " + (String(g$1) + (", " + (String(b$1) + (", " + (string_of_float(a$1) + ")")))))));
      }
    }
  }

  function string_of_length(param) {
    if (typeof param === "number") {
      return "0";
    } else {
      var variant = param[0];
      if (variant >= 22632) {
        if (variant >= 25096) {
          if (variant >= 26433) {
            if (variant >= 5691738) {
              return string_of_float(param[1]) + "rem";
            } else {
              return string_of_float(param[1]) + "vw";
            }
          } else if (variant >= 26418) {
            return string_of_float(param[1]) + "vh";
          } else {
            return String(param[1]) + "px";
          }
        } else if (variant >= 24416) {
          if (variant >= 25092) {
            return String(param[1]) + "pt";
          } else {
            return string_of_float(param[1]) + "mm";
          }
        } else if (variant >= 22643) {
          return string_of_float(param[1]) + "ex";
        } else {
          return string_of_float(param[1]) + "em";
        }
      } else if (variant >= -833470756) {
        if (variant >= 22181) {
          if (variant >= 22186) {
            return string_of_float(param[1]) + "cm";
          } else {
            return string_of_float(param[1]) + "ch";
          }
        } else if (variant >= -119887163) {
          return string_of_float(param[1]) + "%";
        } else {
          return string_of_float(param[1]) + "vmin";
        }
      } else if (variant !== -1040484748) {
        if (variant >= -833472530) {
          return string_of_float(param[1]) + "vmax";
        } else {
          var match = param[1];
          if (match[0] >= 5745024) {
            return "calc(" + (string_of_length(match[1]) + (" - " + (string_of_length(match[2]) + ")")));
          } else {
            return "calc(" + (string_of_length(match[1]) + (" + " + (string_of_length(match[2]) + ")")));
          }
        }
      } else {
        return string_of_float(param[1]) + "px";
      }
    }
  }

  function string_of_overflow(param) {
    if (param >= -862584982) {
      if (param >= 589592690) {
        return "visible";
      } else {
        return "hidden";
      }
    } else if (param >= -949692403) {
      return "scroll";
    } else {
      return "auto";
    }
  }

  function string_of_cursor(x) {
    if (x >= 82328943) {
      if (x >= 477181167) {
        if (x >= 624364317) {
          if (x !== 626862894) {
            if (x >= 939907157) {
              if (x >= 951366027) {
                return "ne-resize";
              } else {
                return "not-allowed";
              }
            } else if (x >= 676920916) {
              return "col-resize";
            } else {
              return "nw-resize";
            }
          } else {
            return "context-menu";
          }
        } else if (x >= 577832966) {
          if (x >= 621258809) {
            return "e-resize";
          } else {
            return "ew-resize";
          }
        } else if (x >= 563171728) {
          return "alias";
        } else {
          return "nesw-resize";
        }
      } else if (x >= 302348560) {
        if (x !== 365450254) {
          if (x >= 457757954) {
            if (x >= 465819841) {
              return "default";
            } else {
              return "n-resize";
            }
          } else if (x >= 381626435) {
            return "vertical-text";
          } else {
            return "no-drop";
          }
        } else {
          return "all-scroll";
        }
      } else if (x >= 180897442) {
        if (x >= 294257099) {
          return "w-resize";
        } else {
          return "crosshair";
        }
      } else if (x >= 103479213) {
        return "progress";
      } else {
        return "nwse-resize";
      }
    } else if (x >= -840286290) {
      if (x >= -459627717) {
        if (x !== -348903737) {
          if (x >= -19620980) {
            if (x >= -18796519) {
              return "ns-resize";
            } else {
              return "grabbing";
            }
          } else if (x >= -88732200) {
            return "zoom-in";
          } else {
            return "zoom-out";
          }
        } else {
          return "s-resize";
        }
      } else if (x >= -786317123) {
        if (x >= -693048282) {
          return "se-resize";
        } else {
          return "pointer";
        }
      } else if (x >= -822977931) {
        return "wait";
      } else {
        return "row-resize";
      }
    } else if (x >= -999567412) {
      if (x !== -989121855) {
        if (x >= -922086728) {
          if (x >= -856044371) {
            return "text";
          } else {
            return "none";
          }
        } else if (x >= -933174511) {
          return "move";
        } else {
          return "grab";
        }
      } else {
        return "help";
      }
    } else if (x >= -1044071499) {
      if (x >= -1020049992) {
        return "sw-resize";
      } else {
        return "copy";
      }
    } else if (x >= -1044569694) {
      return "cell";
    } else {
      return "auto";
    }
  }

  function string_of_fontWeight(x) {
    if (typeof x === "number") {
      if (x >= -81801163) {
        if (x >= 434326211) {
          if (x >= 812216871) {
            if (x >= 888264127) {
              return "900";
            } else {
              return "400";
            }
          } else if (x >= 653193961) {
            return "unset";
          } else {
            return "lighter";
          }
        } else if (x !== -21160922) {
          if (x >= -20425611) {
            return "500";
          } else {
            return "800";
          }
        } else {
          return "200";
        }
      } else if (x >= -812709613) {
        if (x >= -534575822) {
          if (x >= -184774442) {
            return "300";
          } else {
            return "bolder";
          }
        } else if (x >= -601204732) {
          return "inherit";
        } else {
          return "600";
        }
      } else if (x !== -878767996) {
        if (x >= -855898535) {
          return "100";
        } else {
          return "700";
        }
      } else {
        return "initial";
      }
    } else {
      return String(x[1]);
    }
  }

  function d(property, value) {
    return /* `declaration */[
            -434952966,
            /* tuple */[
              property,
              value
            ]
          ];
  }

  function rgb(r, g, b) {
    return /* `rgb */[
            5692173,
            /* tuple */[
              r,
              g,
              b
            ]
          ];
  }

  function hex(x) {
    return /* `hex */[
            5194459,
            x
          ];
  }

  function string_of_length_cascading(param) {
    if (typeof param === "number") {
      if (param >= -601204732) {
        if (param >= 653193961) {
          return "unset";
        } else {
          return "inherit";
        }
      } else if (param >= -789508312) {
        return "0";
      } else {
        return "initial";
      }
    } else {
      var variant = param[0];
      if (variant >= 22632) {
        if (variant >= 25096) {
          if (variant >= 26433) {
            if (variant >= 5691738) {
              return string_of_float(param[1]) + "rem";
            } else {
              return string_of_float(param[1]) + "vw";
            }
          } else if (variant >= 26418) {
            return string_of_float(param[1]) + "vh";
          } else {
            return String(param[1]) + "px";
          }
        } else if (variant >= 24416) {
          if (variant >= 25092) {
            return String(param[1]) + "pt";
          } else {
            return string_of_float(param[1]) + "mm";
          }
        } else if (variant >= 22643) {
          return string_of_float(param[1]) + "ex";
        } else {
          return string_of_float(param[1]) + "em";
        }
      } else if (variant >= -833470756) {
        if (variant >= 22181) {
          if (variant >= 22186) {
            return string_of_float(param[1]) + "cm";
          } else {
            return string_of_float(param[1]) + "ch";
          }
        } else if (variant >= -119887163) {
          return string_of_float(param[1]) + "%";
        } else {
          return string_of_float(param[1]) + "vmin";
        }
      } else if (variant !== -1040484748) {
        if (variant >= -833472530) {
          return string_of_float(param[1]) + "vmax";
        } else {
          var match = param[1];
          if (match[0] >= 5745024) {
            return "calc(" + (string_of_length(match[1]) + (" - " + (string_of_length(match[2]) + ")")));
          } else {
            return "calc(" + (string_of_length(match[1]) + (" + " + (string_of_length(match[2]) + ")")));
          }
        }
      } else {
        return string_of_float(param[1]) + "px";
      }
    }
  }

  function em(x) {
    return /* `em */[
            22632,
            x
          ];
  }

  function px(x) {
    return /* `px */[
            25096,
            x
          ];
  }

  function display(x) {
    return d("display", x >= 64712127 ? (
                  x >= 653193961 ? (
                      x >= 793912528 ? (
                          x >= 888960333 ? (
                              x >= 1054826616 ? "table-caption" : "block"
                            ) : (
                              x >= 850209563 ? "table-column-group" : "table-cell"
                            )
                        ) : (
                          x >= 790889754 ? "contents" : "unset"
                        )
                    ) : (
                      x >= 182695950 ? (
                          x >= 606419204 ? (
                              x >= 632591505 ? "list-item" : "table-header-group"
                            ) : (
                              x >= 423610969 ? "inline" : "table"
                            )
                        ) : (
                          x >= 97122692 ? "table-column" : "inline-grid"
                        )
                    )
                ) : (
                  x >= -843129172 ? (
                      x >= -601204732 ? (
                          x >= -147785676 ? (
                              x >= 53323314 ? "inline-flex" : "inline-block"
                            ) : (
                              x >= -245903344 ? "run-in" : "inherit"
                            )
                        ) : (
                          x >= -836725517 ? "table-row-group" : "table-row"
                        )
                    ) : (
                      x >= -999565626 ? (
                          x >= -878767996 ? (
                              x >= -854050059 ? "inline-table" : "initial"
                            ) : (
                              x >= -922086728 ? "none" : "grid"
                            )
                        ) : (
                          x >= -1010954439 ? "flex" : "table-footer-group"
                        )
                    )
                ));
  }

  function position(x) {
    return d("position", x >= 100392110 ? (
                  x >= 653193961 ? (
                      x >= 903134412 ? "relative" : "unset"
                    ) : (
                      x >= 188263721 ? "sticky" : "static"
                    )
                ) : (
                  x >= -601204732 ? (
                      x >= 10615156 ? "fixed" : "inherit"
                    ) : (
                      x >= -878767996 ? "initial" : "absolute"
                    )
                ));
  }

  function top(x) {
    return d("top", string_of_length(x));
  }

  function flexGrow(x) {
    return d("flexGrow", string_of_float(x));
  }

  function flexShrink(x) {
    return d("flexShrink", String(x));
  }

  function flexBasis(x) {
    var tmp;
    if (typeof x === "number") {
      tmp = x >= -550577721 ? (
          x >= 60557045 ? (
              x >= 427265337 ? "content" : "max-content"
            ) : (
              x >= -195805336 ? "fit-content" : "min-content"
            )
        ) : (
          x !== -1011102077 ? (
              x >= -789508312 ? "0" : "auto"
            ) : "fill"
        );
    } else {
      var variant = x[0];
      if (variant >= 22632) {
        tmp = variant >= 25096 ? (
            variant >= 26433 ? (
                variant >= 5691738 ? string_of_float(x[1]) + "rem" : string_of_float(x[1]) + "vw"
              ) : (
                variant >= 26418 ? string_of_float(x[1]) + "vh" : String(x[1]) + "px"
              )
          ) : (
            variant >= 24416 ? (
                variant >= 25092 ? String(x[1]) + "pt" : string_of_float(x[1]) + "mm"
              ) : (
                variant >= 22643 ? string_of_float(x[1]) + "ex" : string_of_float(x[1]) + "em"
              )
          );
      } else if (variant >= -833470756) {
        tmp = variant >= 22181 ? (
            variant >= 22186 ? string_of_float(x[1]) + "cm" : string_of_float(x[1]) + "ch"
          ) : (
            variant >= -119887163 ? string_of_float(x[1]) + "%" : string_of_float(x[1]) + "vmin"
          );
      } else if (variant !== -1040484748) {
        if (variant >= -833472530) {
          tmp = string_of_float(x[1]) + "vmax";
        } else {
          var match = x[1];
          tmp = match[0] >= 5745024 ? "calc(" + (string_of_length(match[1]) + (" - " + (string_of_length(match[2]) + ")"))) : "calc(" + (string_of_length(match[1]) + (" + " + (string_of_length(match[2]) + ")")));
        }
      } else {
        tmp = string_of_float(x[1]) + "px";
      }
    }
    return d("flexBasis", tmp);
  }

  function string_of_margin(param) {
    if (typeof param === "number") {
      if (param >= -789508312) {
        return "0";
      } else {
        return "auto";
      }
    } else {
      var variant = param[0];
      if (variant >= 22632) {
        if (variant >= 25096) {
          if (variant >= 26433) {
            if (variant >= 5691738) {
              return string_of_float(param[1]) + "rem";
            } else {
              return string_of_float(param[1]) + "vw";
            }
          } else if (variant >= 26418) {
            return string_of_float(param[1]) + "vh";
          } else {
            return String(param[1]) + "px";
          }
        } else if (variant >= 24416) {
          if (variant >= 25092) {
            return String(param[1]) + "pt";
          } else {
            return string_of_float(param[1]) + "mm";
          }
        } else if (variant >= 22643) {
          return string_of_float(param[1]) + "ex";
        } else {
          return string_of_float(param[1]) + "em";
        }
      } else if (variant >= -833470756) {
        if (variant >= 22181) {
          if (variant >= 22186) {
            return string_of_float(param[1]) + "cm";
          } else {
            return string_of_float(param[1]) + "ch";
          }
        } else if (variant >= -119887163) {
          return string_of_float(param[1]) + "%";
        } else {
          return string_of_float(param[1]) + "vmin";
        }
      } else if (variant !== -1040484748) {
        if (variant >= -833472530) {
          return string_of_float(param[1]) + "vmax";
        } else {
          var match = param[1];
          if (match[0] >= 5745024) {
            return "calc(" + (string_of_length(match[1]) + (" - " + (string_of_length(match[2]) + ")")));
          } else {
            return "calc(" + (string_of_length(match[1]) + (" + " + (string_of_length(match[2]) + ")")));
          }
        }
      } else {
        return string_of_float(param[1]) + "px";
      }
    }
  }

  function margin(x) {
    return d("margin", string_of_margin(x));
  }

  function marginTop(x) {
    return d("marginTop", string_of_margin(x));
  }

  function marginBottom(x) {
    return d("marginBottom", string_of_margin(x));
  }

  function padding(x) {
    return d("padding", string_of_length(x));
  }

  function paddingTop(x) {
    return d("paddingTop", string_of_length(x));
  }

  function paddingBottom(x) {
    return d("paddingBottom", string_of_length(x));
  }

  function string_of_minmax(param) {
    if (typeof param === "number") {
      if (param >= -550577721) {
        if (param >= 60557045) {
          return "max-content";
        } else {
          return "min-content";
        }
      } else if (param >= -789508312) {
        return "0";
      } else {
        return "auto";
      }
    } else {
      var variant = param[0];
      if (variant >= 22643) {
        if (variant >= 25096) {
          if (variant >= 26433) {
            if (variant >= 5691738) {
              return string_of_float(param[1]) + "rem";
            } else {
              return string_of_float(param[1]) + "vw";
            }
          } else if (variant >= 26418) {
            return string_of_float(param[1]) + "vh";
          } else {
            return String(param[1]) + "px";
          }
        } else if (variant >= 24416) {
          if (variant >= 25092) {
            return String(param[1]) + "pt";
          } else {
            return string_of_float(param[1]) + "mm";
          }
        } else if (variant >= 22860) {
          return string_of_float(param[1]) + "fr";
        } else {
          return string_of_float(param[1]) + "ex";
        }
      } else if (variant >= -119887163) {
        if (variant >= 22186) {
          if (variant >= 22632) {
            return string_of_float(param[1]) + "em";
          } else {
            return string_of_float(param[1]) + "cm";
          }
        } else if (variant >= 22181) {
          return string_of_float(param[1]) + "ch";
        } else {
          return string_of_float(param[1]) + "%";
        }
      } else if (variant >= -833472530) {
        if (variant >= -833470756) {
          return string_of_float(param[1]) + "vmin";
        } else {
          return string_of_float(param[1]) + "vmax";
        }
      } else if (variant >= -1040484748) {
        return string_of_float(param[1]) + "px";
      } else {
        var match = param[1];
        if (match[0] >= 5745024) {
          return "calc(" + (string_of_length(match[1]) + (" - " + (string_of_length(match[2]) + ")")));
        } else {
          return "calc(" + (string_of_length(match[1]) + (" + " + (string_of_length(match[2]) + ")")));
        }
      }
    }
  }

  function string_of_dimension(param) {
    if (typeof param === "number") {
      if (param !== -922086728) {
        if (param >= -550577721) {
          if (param >= 60557045) {
            return "max-content";
          } else {
            return "min-content";
          }
        } else if (param >= -789508312) {
          return "0";
        } else {
          return "auto";
        }
      } else {
        return "none";
      }
    } else {
      var variant = param[0];
      if (variant >= 22632) {
        if (variant >= 25092) {
          if (variant !== 25096) {
            if (variant >= 26433) {
              if (variant >= 5691738) {
                return string_of_float(param[1]) + "rem";
              } else {
                return string_of_float(param[1]) + "vw";
              }
            } else if (variant >= 26418) {
              return string_of_float(param[1]) + "vh";
            } else {
              return String(param[1]) + "pt";
            }
          } else {
            return String(param[1]) + "px";
          }
        } else if (variant >= 22860) {
          if (variant >= 24416) {
            return string_of_float(param[1]) + "mm";
          } else {
            return string_of_float(param[1]) + "fr";
          }
        } else if (variant >= 22643) {
          return string_of_float(param[1]) + "ex";
        } else {
          return string_of_float(param[1]) + "em";
        }
      } else if (variant >= -754859950) {
        if (variant >= 22181) {
          if (variant >= 22186) {
            return string_of_float(param[1]) + "cm";
          } else {
            return string_of_float(param[1]) + "ch";
          }
        } else if (variant >= -119887163) {
          return string_of_float(param[1]) + "%";
        } else {
          var match = param[1];
          return "minmax(" + (string_of_minmax(match[0]) + ("," + (string_of_minmax(match[1]) + ")")));
        }
      } else if (variant >= -833472530) {
        if (variant >= -833470756) {
          return string_of_float(param[1]) + "vmin";
        } else {
          return string_of_float(param[1]) + "vmax";
        }
      } else if (variant >= -1040484748) {
        return string_of_float(param[1]) + "px";
      } else {
        var match$1 = param[1];
        if (match$1[0] >= 5745024) {
          return "calc(" + (string_of_length(match$1[1]) + (" - " + (string_of_length(match$1[2]) + ")")));
        } else {
          return "calc(" + (string_of_length(match$1[1]) + (" + " + (string_of_length(match$1[2]) + ")")));
        }
      }
    }
  }

  function width(x) {
    return d("width", string_of_dimension(x));
  }

  function maxHeight(x) {
    return d("maxHeight", string_of_dimension(x));
  }

  function string_of_align(param) {
    if (param >= 98248149) {
      if (param >= 662439529) {
        if (param >= 924268066) {
          return "flex-end";
        } else {
          return "flex-start";
        }
      } else if (param >= 287825029) {
        return "baseline";
      } else {
        return "center";
      }
    } else if (param >= -162316795) {
      return "stretch";
    } else {
      return "auto";
    }
  }

  function alignItems(x) {
    return d("alignItems", string_of_align(x));
  }

  function string_of_justify(param) {
    if (param >= 98248149) {
      if (param >= 662439529) {
        if (param >= 924268066) {
          return "flex-end";
        } else {
          return "flex-start";
        }
      } else if (param >= 516682146) {
        return "space-between";
      } else {
        return "center";
      }
    } else if (param !== -485895757) {
      if (param >= -162316795) {
        return "stretch";
      } else {
        return "space-evenly";
      }
    } else {
      return "space-around";
    }
  }

  function justifyContent(x) {
    return d("justifyContent", string_of_justify(x));
  }

  function overflow(x) {
    return d("overflow", string_of_overflow(x));
  }

  function string_of_borderstyle(param) {
    if (param >= 568403505) {
      if (param >= 841979626) {
        return "dotted";
      } else {
        return "dashed";
      }
    } else if (param >= 12956715) {
      return "solid";
    } else {
      return "none";
    }
  }

  function border(px, style, color) {
    return d("border", string_of_length(px) + (" " + (string_of_borderstyle(style) + (" " + string_of_color(color)))));
  }

  function backgroundColor(x) {
    return d("backgroundColor", string_of_color(x));
  }

  function cursor(x) {
    return d("cursor", string_of_cursor(x));
  }

  function string_of_listStyleType(param) {
    if (param >= -484197732) {
      if (param >= 700345660) {
        if (param >= 787279419) {
          if (param >= 826920258) {
            return "upper-latin";
          } else {
            return "upper-roman";
          }
        } else if (param >= 739986499) {
          return "lower-latin";
        } else {
          return "lower-roman";
        }
      } else if (param >= -422333295) {
        return "decimal";
      } else {
        return "upper-alpha";
      }
    } else if (param >= -703761904) {
      if (param >= -655228771) {
        if (param >= -571131491) {
          return "lower-alpha";
        } else {
          return "square";
        }
      } else if (param >= -699686657) {
        return "lower-greek";
      } else {
        return "circle";
      }
    } else if (param >= -922086728) {
      return "none";
    } else {
      return "disc";
    }
  }

  function listStyleType(x) {
    return d("listStyleType", string_of_listStyleType(x));
  }

  function string_of_outlineStyle(param) {
    if (param >= 472095738) {
      if (param !== 568403505) {
        if (param >= 852175633) {
          if (param >= 1042283741) {
            return "inset";
          } else {
            return "double";
          }
        } else if (param >= 841979626) {
          return "dotted";
        } else {
          return "grove";
        }
      } else {
        return "dashed";
      }
    } else if (param !== -862584982) {
      if (param >= 12956715) {
        if (param >= 209930196) {
          return "outset";
        } else {
          return "solid";
        }
      } else if (param >= -379468757) {
        return "ridge";
      } else {
        return "none";
      }
    } else {
      return "hidden";
    }
  }

  function outline(size, style, color) {
    return d("outline", string_of_length(size) + (" " + (string_of_outlineStyle(style) + (" " + string_of_color(color)))));
  }

  function color(x) {
    return d("color", string_of_color(x));
  }

  function fontSize(x) {
    return d("fontSize", string_of_length_cascading(x));
  }

  function fontWeight(x) {
    return d("fontWeight", string_of_fontWeight(x));
  }

  function selector(selector$1, rules) {
    return /* `selector */[
            829240095,
            /* tuple */[
              selector$1,
              rules
            ]
          ];
  }

  var style = make;

  var black$1 = black;

  var green$1 = green;

  var white$1 = white;

  var zero = /* zero */-789508312;

  var unsafe = d;
  /* emotion Not a pure module */

  // Generated by BUCKLESCRIPT VERSION 5.0.3, PLEASE EDIT WITH CARE

  var palette = style(/* :: */[
        position(/* fixed */10615156),
        /* :: */[
          top(zero),
          /* :: */[
            width(/* `percent */[
                  -119887163,
                  100.0
                ]),
            /* :: */[
              display(/* flex */-1010954439),
              /* :: */[
                alignItems(/* center */98248149),
                /* :: */[
                  justifyContent(/* center */98248149),
                  /* :: */[
                    selector(" > div", /* :: */[
                          flexGrow(0.0),
                          /* :: */[
                            flexShrink(0),
                            /* :: */[
                              flexBasis(px(500)),
                              /* :: */[
                                unsafe("boxShadow", "0 8px 10px 0 rgba(0, 0, 0, 0.14), 0 3px 14px 2px rgba(0, 0, 0, 0.12), 0 5px 5px -3px rgba(0, 0, 0, 0.2)"),
                                /* :: */[
                                  selector(" > div > ul", /* :: */[
                                        maxHeight(px(200)),
                                        /* [] */0
                                      ]),
                                  /* [] */0
                                ]
                              ]
                            ]
                          ]
                        ]),
                    /* [] */0
                  ]
                ]
              ]
            ]
          ]
        ]
      ]);

  var search = style(/* :: */[
        backgroundColor(hex("f3f3f3")),
        /* :: */[
          paddingTop(px(1)),
          /* :: */[
            paddingBottom(px(1)),
            /* [] */0
          ]
        ]
      ]);

  var inputWrapper = style(/* :: */[
        padding(px(4)),
        /* :: */[
          margin(px(4)),
          /* :: */[
            border(px(1), /* solid */12956715, hex("99cbe9")),
            /* :: */[
              backgroundColor(white$1),
              /* [] */0
            ]
          ]
        ]
      ]);

  var input = style(/* :: */[
        display(/* block */888960333),
        /* :: */[
          width(/* `percent */[
                -119887163,
                100.0
              ]),
          /* :: */[
            padding(zero),
            /* :: */[
              margin(zero),
              /* :: */[
                border(px(0), /* none */-922086728, /* transparent */582626130),
                /* :: */[
                  outline(px(0), /* none */-922086728, /* transparent */582626130),
                  /* :: */[
                    backgroundColor(/* transparent */582626130),
                    /* :: */[
                      color(black$1),
                      /* [] */0
                    ]
                  ]
                ]
              ]
            ]
          ]
        ]
      ]);

  var results = style(/* :: */[
        overflow(/* auto */-1065951377),
        /* :: */[
          listStyleType(/* none */-922086728),
          /* :: */[
            padding(zero),
            /* :: */[
              margin(zero),
              /* [] */0
            ]
          ]
        ]
      ]);

  function result(isSelected) {
    return style(/* :: */[
                backgroundColor(isSelected ? hex("ccecfe") : /* transparent */582626130),
                /* :: */[
                  fontSize(px(14)),
                  /* :: */[
                    selector("p", /* :: */[
                          marginTop(em(0.3)),
                          /* :: */[
                            marginBottom(em(0.3)),
                            /* [] */0
                          ]
                        ]),
                    /* :: */[
                      padding(px(5)),
                      /* :: */[
                        cursor(/* pointer */-786317123),
                        /* :: */[
                          selector("&:hover", /* :: */[
                                backgroundColor(isSelected ? hex("ccecfe") : hex("e7e7e7")),
                                /* [] */0
                              ]),
                          /* [] */0
                        ]
                      ]
                    ]
                  ]
                ]
              ]);
  }

  var title = style(/* :: */[
        fontWeight(/* normal */812216871),
        /* :: */[
          fontSize(px(14)),
          /* :: */[
            margin(zero),
            /* :: */[
              color(hex("616161")),
              /* [] */0
            ]
          ]
        ]
      ]);

  var url = style(/* :: */[
        color(green$1),
        /* :: */[
          fontSize(px(12)),
          /* [] */0
        ]
      ]);

  var snippet = style(/* :: */[
        color(rgb(84, 84, 84)),
        /* :: */[
          fontSize(px(12)),
          /* [] */0
        ]
      ]);
  /* palette Not a pure module */

  // Generated by BUCKLESCRIPT VERSION 5.0.3, PLEASE EDIT WITH CARE

  function Search(Props) {
    var dataSource = Props.dataSource;
    var placeholder = Props.placeholder;
    var search$1 = Props.search;
    var renderItem = Props.renderItem;
    var onClick = Props.onClick;
    var match = React.useState((function () {
            return "";
          }));
    var setValue = match[1];
    var value = match[0];
    var match$1 = React.useState((function () {
            return _2(search$1, dataSource, value);
          }));
    var setResults = match$1[1];
    var results$1 = match$1[0];
    var match$2 = React.useState((function () {
            return undefined;
          }));
    var setSelectedIndex = match$2[1];
    var selectedIndex = match$2[0];
    var listRef = React.useRef(null);
    var selectedItemRef = React.useRef(null);
    React.useEffect((function () {
            _1(setSelectedIndex, (function (param) {
                    var match = results$1.length;
                    if (match !== 0) {
                      return 0;
                    }
                    
                  }));
            return undefined;
          }), /* array */[results$1]);
    React.useLayoutEffect((function () {
            map$1(nullable_to_opt(listRef.current), (function (listEl) {
                    map$1(nullable_to_opt(selectedItemRef.current), (function (childEl) {
                            return scrollIntoView(listEl, childEl);
                          }));
                    return /* () */0;
                  }));
            return undefined;
          }), /* array */[selectedIndex]);
    var onChange = function ($$event) {
      var value = $$event.target.value;
      _1(setValue, value);
      return _1(setResults, (function (param) {
                    return _2(search$1, dataSource, value);
                  }));
    };
    var onKeyDown = function ($$event) {
      $$event.persist();
      var keyCode = getKeyCode($$event.keyCode);
      switch (keyCode) {
        case 0 : 
            return _1(setSelectedIndex, (function (current) {
                          if (current !== undefined) {
                            var index = current;
                            var match = get(results$1, index + 1 | 0);
                            if (match !== undefined) {
                              return index + 1 | 0;
                            } else {
                              return index;
                            }
                          } else {
                            return 0;
                          }
                        }));
        case 1 : 
            return _1(setSelectedIndex, (function (current) {
                          if (current !== undefined) {
                            var index = current;
                            var match = get(results$1, index - 1 | 0);
                            if (match !== undefined) {
                              return index - 1 | 0;
                            } else {
                              return index;
                            }
                          } else {
                            return results$1.length - 1 | 0;
                          }
                        }));
        case 2 : 
            if (selectedIndex !== undefined) {
              return _1(onClick, getExn(results$1, selectedIndex));
            } else {
              return /* () */0;
            }
        case 3 : 
        case 4 : 
            return /* () */0;
        
      }
    };
    var tmp;
    tmp = results$1.length !== 0 ? React.createElement("ul", {
            ref: listRef,
            className: results,
            tabIndex: -1
          }, mapWithIndex(results$1, (function (index, result$1) {
                  var isSelected = selectedIndex !== undefined ? selectedIndex === index : false;
                  if (isSelected) {
                    return React.createElement("li", {
                                key: String(index),
                                ref: selectedItemRef,
                                className: result(isSelected),
                                onClick: (function (param) {
                                    return _1(onClick, getExn(results$1, index));
                                  })
                              }, _2(renderItem, dataSource, result$1));
                  } else {
                    return React.createElement("li", {
                                key: String(index),
                                className: result(isSelected),
                                onClick: (function (param) {
                                    return _1(onClick, getExn(results$1, index));
                                  })
                              }, _2(renderItem, dataSource, result$1));
                  }
                }))) : (
        value === "" ? React.createElement("div", undefined) : React.createElement("ul", {
                className: results,
                tabIndex: -1
              }, React.createElement("li", {
                    className: result(true)
                  }, React.createElement("p", {
                        className: snippet
                      }, "No result for ", React.createElement("em", undefined, value))))
      );
    return React.createElement("div", {
                className: search,
                onKeyDown: onKeyDown
              }, React.createElement("div", {
                    className: inputWrapper
                  }, React.createElement("input", {
                        className: input,
                        autoFocus: true,
                        placeholder: placeholder,
                        type: "text",
                        value: value,
                        onChange: onChange
                      })), tmp);
  }

  var make$1 = Search;
  /* react Not a pure module */

  var Search_bs = /*#__PURE__*/Object.freeze({
    make: make$1
  });

  function fromArray(a) {
    var a$1 = a;
    var _i = a.length - 1 | 0;
    var _res = /* [] */0;
    while(true) {
      var res = _res;
      var i = _i;
      if (i < 0) {
        return res;
      } else {
        _res = /* :: */[
          a$1[i],
          res
        ];
        _i = i - 1 | 0;
        continue ;
      }
    }}

  function getByU(_xs, p) {
    while(true) {
      var xs = _xs;
      if (xs) {
        var x = xs[0];
        if (p(x)) {
          return some(x);
        } else {
          _xs = xs[1];
          continue ;
        }
      } else {
        return undefined;
      }
    }}

  function getBy(xs, p) {
    return getByU(xs, __1(p));
  }
  /* No side effect */

  // Generated by BUCKLESCRIPT VERSION 5.0.3, PLEASE EDIT WITH CARE

  function Palette(Props) {
    var config = Props.config;
    var match = React.useState((function () {
            return undefined;
          }));
    var setActiveItem = match[1];
    var activeItem = match[0];
    var containerRef = React.useRef(null);
    var listConfig = fromArray(config);
    var $$document$1 = document;
    var match$1 = useEventListener($$document$1, "keydown", (function (e) {
            var key = e.key;
            if (e.altKey && key !== "Alt") {
              return _1(setActiveItem, (function (param) {
                            var item = getBy(listConfig, (function (item) {
                                    return key === item.key;
                                  }));
                            if (item !== undefined) {
                              e.preventDefault();
                            }
                            return item;
                          }));
            } else {
              return 0;
            }
          }));
    var match$2 = useEventListener($$document$1, "keydown", (function (e) {
            var key = e.key;
            if (key === "Escape") {
              return _1(setActiveItem, (function (param) {
                            return undefined;
                          }));
            } else {
              return 0;
            }
          }));
    var match$3 = useEventListener($$document$1, "mousedown", (function (e) {
            map$1(nullable_to_opt(containerRef.current), (function (searchElement) {
                    var clickedElement = e.target;
                    if (!searchElement.contains(clickedElement)) {
                      _1(setActiveItem, (function (param) {
                              return undefined;
                            }));
                    }
                    return undefined;
                  }));
            return /* () */0;
          }));
    if (activeItem !== undefined) {
      _1(match$1[1], /* () */0);
      _1(match$2[0], /* () */0);
      _1(match$3[0], /* () */0);
    } else {
      _1(match$2[1], /* () */0);
      _1(match$3[1], /* () */0);
      _1(match$1[0], /* () */0);
    }
    return React.createElement("div", {
                ref: containerRef
              }, activeItem !== undefined ? valFromOption(activeItem).component : React.createElement("span", undefined));
  }

  var make$2 = Palette;
  /* react Not a pure module */

  var Palette_bs = /*#__PURE__*/Object.freeze({
    make: make$2
  });

  function treeHeight(n) {
    if (n !== null) {
      return n.height;
    } else {
      return 0;
    }
  }

  function create$1(l, v, r) {
    var hl = l !== null ? l.height : 0;
    var hr = r !== null ? r.height : 0;
    return {
            value: v,
            height: hl >= hr ? hl + 1 | 0 : hr + 1 | 0,
            left: l,
            right: r
          };
  }

  function singleton(x) {
    return {
            value: x,
            height: 1,
            left: null,
            right: null
          };
  }

  function heightGe(l, r) {
    if (r !== null) {
      if (l !== null) {
        return l.height >= r.height;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  function bal(l, v, r) {
    var hl = l !== null ? l.height : 0;
    var hr = r !== null ? r.height : 0;
    if (hl > (hr + 2 | 0)) {
      var ll = l.left;
      var lv = l.value;
      var lr = l.right;
      if (heightGe(ll, lr)) {
        return create$1(ll, lv, create$1(lr, v, r));
      } else {
        var lrl = lr.left;
        var lrv = lr.value;
        var lrr = lr.right;
        return create$1(create$1(ll, lv, lrl), lrv, create$1(lrr, v, r));
      }
    } else if (hr > (hl + 2 | 0)) {
      var rl = r.left;
      var rv = r.value;
      var rr = r.right;
      if (heightGe(rr, rl)) {
        return create$1(create$1(l, v, rl), rv, rr);
      } else {
        var rll = rl.left;
        var rlv = rl.value;
        var rlr = rl.right;
        return create$1(create$1(l, v, rll), rlv, create$1(rlr, rv, rr));
      }
    } else {
      return {
              value: v,
              height: hl >= hr ? hl + 1 | 0 : hr + 1 | 0,
              left: l,
              right: r
            };
    }
  }

  function removeMinAuxWithRef(n, v) {
    var ln = n.left;
    var rn = n.right;
    var kn = n.value;
    if (ln !== null) {
      return bal(removeMinAuxWithRef(ln, v), kn, rn);
    } else {
      v[0] = kn;
      return rn;
    }
  }

  function addMinElement(n, v) {
    if (n !== null) {
      return bal(addMinElement(n.left, v), n.value, n.right);
    } else {
      return singleton(v);
    }
  }

  function addMaxElement(n, v) {
    if (n !== null) {
      return bal(n.left, n.value, addMaxElement(n.right, v));
    } else {
      return singleton(v);
    }
  }

  function joinShared(ln, v, rn) {
    if (ln !== null) {
      if (rn !== null) {
        var lh = ln.height;
        var rh = rn.height;
        if (lh > (rh + 2 | 0)) {
          return bal(ln.left, ln.value, joinShared(ln.right, v, rn));
        } else if (rh > (lh + 2 | 0)) {
          return bal(joinShared(ln, v, rn.left), rn.value, rn.right);
        } else {
          return create$1(ln, v, rn);
        }
      } else {
        return addMaxElement(ln, v);
      }
    } else {
      return addMinElement(rn, v);
    }
  }

  function concatShared(t1, t2) {
    if (t1 !== null) {
      if (t2 !== null) {
        var v = /* record */[/* contents */t2.value];
        var t2r = removeMinAuxWithRef(t2, v);
        return joinShared(t1, v[0], t2r);
      } else {
        return t1;
      }
    } else {
      return t2;
    }
  }

  function lengthNode(n) {
    var l = n.left;
    var r = n.right;
    var sizeL = l !== null ? lengthNode(l) : 0;
    var sizeR = r !== null ? lengthNode(r) : 0;
    return (1 + sizeL | 0) + sizeR | 0;
  }

  function fillArray(_n, _i, arr) {
    while(true) {
      var i = _i;
      var n = _n;
      var l = n.left;
      var v = n.value;
      var r = n.right;
      var next = l !== null ? fillArray(l, i, arr) : i;
      arr[next] = v;
      var rnext = next + 1 | 0;
      if (r !== null) {
        _i = rnext;
        _n = r;
        continue ;
      } else {
        return rnext;
      }
    }}

  function toArray(n) {
    if (n !== null) {
      var size = lengthNode(n);
      var v = new Array(size);
      fillArray(n, 0, v);
      return v;
    } else {
      return /* array */[];
    }
  }

  function fromSortedArrayRevAux(arr, off, len) {
    switch (len) {
      case 0 : 
          return null;
      case 1 : 
          return singleton(arr[off]);
      case 2 : 
          var x0 = arr[off];
          var x1 = arr[off - 1 | 0];
          return {
                  value: x1,
                  height: 2,
                  left: singleton(x0),
                  right: null
                };
      case 3 : 
          var x0$1 = arr[off];
          var x1$1 = arr[off - 1 | 0];
          var x2 = arr[off - 2 | 0];
          return {
                  value: x1$1,
                  height: 2,
                  left: singleton(x0$1),
                  right: singleton(x2)
                };
      default:
        var nl = len / 2 | 0;
        var left = fromSortedArrayRevAux(arr, off, nl);
        var mid = arr[off - nl | 0];
        var right = fromSortedArrayRevAux(arr, (off - nl | 0) - 1 | 0, (len - nl | 0) - 1 | 0);
        return create$1(left, mid, right);
    }
  }

  function fromSortedArrayAux(arr, off, len) {
    switch (len) {
      case 0 : 
          return null;
      case 1 : 
          return singleton(arr[off]);
      case 2 : 
          var x0 = arr[off];
          var x1 = arr[off + 1 | 0];
          return {
                  value: x1,
                  height: 2,
                  left: singleton(x0),
                  right: null
                };
      case 3 : 
          var x0$1 = arr[off];
          var x1$1 = arr[off + 1 | 0];
          var x2 = arr[off + 2 | 0];
          return {
                  value: x1$1,
                  height: 2,
                  left: singleton(x0$1),
                  right: singleton(x2)
                };
      default:
        var nl = len / 2 | 0;
        var left = fromSortedArrayAux(arr, off, nl);
        var mid = arr[off + nl | 0];
        var right = fromSortedArrayAux(arr, (off + nl | 0) + 1 | 0, (len - nl | 0) - 1 | 0);
        return create$1(left, mid, right);
    }
  }

  function keepSharedU(n, p) {
    if (n !== null) {
      var l = n.left;
      var v = n.value;
      var r = n.right;
      var newL = keepSharedU(l, p);
      var pv = p(v);
      var newR = keepSharedU(r, p);
      if (pv) {
        if (l === newL && r === newR) {
          return n;
        } else {
          return joinShared(newL, v, newR);
        }
      } else {
        return concatShared(newL, newR);
      }
    } else {
      return null;
    }
  }

  function keepShared(n, p) {
    return keepSharedU(n, __1(p));
  }

  function rotateWithLeftChild(k2) {
    var k1 = k2.left;
    k2.left = k1.right;
    k1.right = k2;
    var hlk2 = treeHeight(k2.left);
    var hrk2 = treeHeight(k2.right);
    k2.height = (
      hlk2 > hrk2 ? hlk2 : hrk2
    ) + 1 | 0;
    var hlk1 = treeHeight(k1.left);
    var hk2 = k2.height;
    k1.height = (
      hlk1 > hk2 ? hlk1 : hk2
    ) + 1 | 0;
    return k1;
  }

  function rotateWithRightChild(k1) {
    var k2 = k1.right;
    k1.right = k2.left;
    k2.left = k1;
    var hlk1 = treeHeight(k1.left);
    var hrk1 = treeHeight(k1.right);
    k1.height = (
      hlk1 > hrk1 ? hlk1 : hrk1
    ) + 1 | 0;
    var hrk2 = treeHeight(k2.right);
    var hk1 = k1.height;
    k2.height = (
      hrk2 > hk1 ? hrk2 : hk1
    ) + 1 | 0;
    return k2;
  }

  function doubleWithLeftChild(k3) {
    var v = rotateWithRightChild(k3.left);
    k3.left = v;
    return rotateWithLeftChild(k3);
  }

  function doubleWithRightChild(k2) {
    var v = rotateWithLeftChild(k2.right);
    k2.right = v;
    return rotateWithRightChild(k2);
  }

  function heightUpdateMutate(t) {
    var hlt = treeHeight(t.left);
    var hrt = treeHeight(t.right);
    t.height = (
      hlt > hrt ? hlt : hrt
    ) + 1 | 0;
    return t;
  }

  function balMutate(nt) {
    var l = nt.left;
    var r = nt.right;
    var hl = treeHeight(l);
    var hr = treeHeight(r);
    if (hl > (2 + hr | 0)) {
      var ll = l.left;
      var lr = l.right;
      if (heightGe(ll, lr)) {
        return heightUpdateMutate(rotateWithLeftChild(nt));
      } else {
        return heightUpdateMutate(doubleWithLeftChild(nt));
      }
    } else if (hr > (2 + hl | 0)) {
      var rl = r.left;
      var rr = r.right;
      if (heightGe(rr, rl)) {
        return heightUpdateMutate(rotateWithRightChild(nt));
      } else {
        return heightUpdateMutate(doubleWithRightChild(nt));
      }
    } else {
      nt.height = (
        hl > hr ? hl : hr
      ) + 1 | 0;
      return nt;
    }
  }

  var empty = null;
  /* No side effect */

  function sortedLengthAuxMore(xs, _prec, _acc, len) {
    while(true) {
      var acc = _acc;
      var prec = _prec;
      if (acc >= len) {
        return acc;
      } else {
        var v = xs[acc];
        if (prec > v) {
          _acc = acc + 1 | 0;
          _prec = v;
          continue ;
        } else {
          return acc;
        }
      }
    }}

  function strictlySortedLength(xs) {
    var len = xs.length;
    if (len === 0 || len === 1) {
      return len;
    } else {
      var x0 = xs[0];
      var x1 = xs[1];
      if (x0 < x1) {
        var xs$1 = xs;
        var _prec = x1;
        var _acc = 2;
        var len$1 = len;
        while(true) {
          var acc = _acc;
          var prec = _prec;
          if (acc >= len$1) {
            return acc;
          } else {
            var v = xs$1[acc];
            if (prec < v) {
              _acc = acc + 1 | 0;
              _prec = v;
              continue ;
            } else {
              return acc;
            }
          }
        }    } else if (x0 > x1) {
        return -sortedLengthAuxMore(xs, x1, 2, len) | 0;
      } else {
        return 1;
      }
    }
  }
  /* No side effect */

  function addMutate(t, x) {
    if (t !== null) {
      var k = t.value;
      if (x === k) {
        return t;
      } else {
        var l = t.left;
        var r = t.right;
        if (x < k) {
          t.left = addMutate(l, x);
        } else {
          t.right = addMutate(r, x);
        }
        return balMutate(t);
      }
    } else {
      return singleton(x);
    }
  }

  function fromArray$1(xs) {
    var len = xs.length;
    if (len === 0) {
      return empty;
    } else {
      var next = strictlySortedLength(xs);
      var result;
      if (next >= 0) {
        result = fromSortedArrayAux(xs, 0, next);
      } else {
        next = -next | 0;
        result = fromSortedArrayRevAux(xs, next - 1 | 0, next);
      }
      for(var i = next ,i_finish = len - 1 | 0; i <= i_finish; ++i){
        result = addMutate(result, xs[i]);
      }
      return result;
    }
  }
  /* No side effect */

  var fromArray$2 = fromArray$1;

  var keep = keepShared;

  var toArray$1 = toArray;
  /* No side effect */

  // Generated by BUCKLESCRIPT VERSION 5.0.3, PLEASE EDIT WITH CARE

  function search$1(dataSource, value) {
    console.log("Seaching fuzzy");
    if (value === "") {
      return toArray$1(dataSource);
    } else {
      return toArray$1(keep(dataSource, (function (item) {
                        return _2(fuzzysearch, value, item);
                      })));
    }
  }

  function renderItem(param, item) {
    return React.createElement("p", undefined, item);
  }

  function onClick(item) {
    return _1(nav, item);
  }

  var placeholder = "Search files...";
  /* react Not a pure module */

  var FuzzyEngine_bs = /*#__PURE__*/Object.freeze({
    placeholder: placeholder,
    search: search$1,
    renderItem: renderItem,
    onClick: onClick
  });

  // Generated by BUCKLESCRIPT VERSION 5.0.3, PLEASE EDIT WITH CARE

  function getDocumentOfItem(dataSource, item) {
    return dataSource.documentStore.docs[item.ref];
  }

  function search$2(dataSource, value) {
    return dataSource.search(value, {
                expand: true,
                bool: "AND"
              });
  }

  function renderItem$1(dataSource, item) {
    var doc = getDocumentOfItem(dataSource, item);
    var match = doc.body.length < 240;
    return React.createElement("div", undefined, React.createElement("h3", {
                    className: title
                  }, doc.title), React.createElement("p", {
                    className: url
                  }, doc.id), React.createElement("p", {
                    className: snippet
                  }, match ? doc.body : sub$1(doc.body, 0, 240)));
  }

  function onClick$1(item) {
    return _1(nav, item.ref);
  }

  var placeholder$1 = "Search content...";
  /* react Not a pure module */

  var LunrEngine_bs = /*#__PURE__*/Object.freeze({
    placeholder: placeholder$1,
    getDocumentOfItem: getDocumentOfItem,
    search: search$2,
    renderItem: renderItem$1,
    onClick: onClick$1
  });

  // Generated by BUCKLESCRIPT VERSION 5.0.3, PLEASE EDIT WITH CARE

  function load(serializedIndex) {
    return Elasticlunr.Index.load(serializedIndex);
  }
  /* elasticlunr Not a pure module */

  // Generated by BUCKLESCRIPT VERSION 5.0.3, PLEASE EDIT WITH CARE

  var getLunrDataSource = load;

  function getFuzzyDataSource(serializedIndex) {
    return fromArray$2(map(Object.keys(load(serializedIndex).documentStore.docs), (function (key) {
                      return key.replace((/#.*$/), "");
                    })));
  }
  /* Lunr-ReactHooksTemplate Not a pure module */



  var index = /*#__PURE__*/Object.freeze({
    fuzzyEngine: FuzzyEngine_bs,
    lunrEngine: LunrEngine_bs,
    getLunrDataSource: getLunrDataSource,
    getFuzzyDataSource: getFuzzyDataSource
  });

  exports.Palette = Palette_bs;
  exports.Search = Search_bs;
  exports.examples = index;
  exports.paletteClassName = palette;

  return exports;

}({}, React, emotion, elasticlunr));
