# search-palette

## Synopsis

This package is built with ReasonML and exports two React components:

- `<Search>`: a text input that'll show results, it takes the following props:

  - `placeholder`: the input's placeholder
  - `search`: the function to execute on user's input
  - `renderItem`: how to render a result item
  - `onClick`: what to do when user selects an item
  - `dataSource`: anything, will be handled by the search function

- `<Palette>`: a keyboard shortcut manager. Props:

  - `config`: a dictionnay to bind a key to a component. Ex: `{'c': MySearchWidget}`

It also exports 2 example _search engines_ (ie. the required payload to instantiate a `Search` component). They live in `src/examples`.

You can see it in action by opening `example/index.html` in your code editor and your
browser.

## Demo

To get a demo, open `example/index.html`, it should work out of the box.
Then press one of the thee shortcut available: `alt + c|f|m`, to start searching
against the dataset.

## Run Project

For develoment, use `example/index.html` with:

```sh
yarn install
yarn start
# in another tab
yarn rollup:iife -- -w
```

After you see the rollup compilation succeed, open up `example/index.html` (**no server needed!**). Then modify whichever `.re` file in `src` and refresh the page to see the changes.

**For more elaborate ReasonReact examples**, please see https://github.com/reasonml-community/reason-react-example

## Build for Production

```sh
yarn clean
yarn build
yarn rollup:esm
```

This will create the artifact `dist/index.esm.js`.
