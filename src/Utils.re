/**
 * Reusable code
 */
[@bs.send]
external eventToKeyboardEvent: Dom.event => Webapi.Dom.KeyboardEvent.t =
  "%identity";

[@bs.send]
external eventToMouseEvent: Dom.event => Webapi.Dom.MouseEvent.t = "%identity";

type keyCodes =
  | ArrowDown
  | ArrowUp
  | Enter
  | Escape
  | Unknown;

let getKeyCode = code => {
  switch (code) {
  | 13 => Enter
  | 27 => Escape
  | 38 => ArrowUp
  | 40 => ArrowDown
  | _ => Unknown
  };
};

/**
 * Dom: scrollIntoView
 *
 * Make sure the parent viewport totally includes the child, otherwise
 * scroll the parent until the child is totally visible
 */
let scrollIntoView = (parentNode, childNode) => {
  let parentRect =
    ReactDOMRe.domElementToObj(parentNode)##getBoundingClientRect();

  let childRect =
    ReactDOMRe.domElementToObj(childNode)##getBoundingClientRect();

  let pt = Webapi.Dom.DomRect.top(parentRect);
  let ph = Webapi.Dom.DomRect.height(parentRect);
  let pb = Webapi.Dom.DomRect.bottom(parentRect);
  let ps = Webapi.Dom.Element.scrollTop(parentNode);

  let ct = Webapi.Dom.DomRect.top(childRect);
  let cb = Webapi.Dom.DomRect.bottom(childRect);
  let ch = Webapi.Dom.DomRect.height(childRect);

  let rt = ct -. pt;

  if (rt < 0.0) {
    parentNode |> Webapi.Dom.Element.scrollTo(0.0, rt +. ps);
  } else if (cb > pb) {
    parentNode |> Webapi.Dom.Element.scrollTo(0.0, rt +. ch -. ph +. ps);
  };
};

/**
 * React hook: useEventListener
 *
 * Uses the Webapi to add/remove an event listener.
 * Event is stored as a React ref otherwise the event
 * cannot be be removed.
 */
let useEventListener = (element, eventName, handler) => {
  let handlerRef = React.useRef(handler);
  (
    () => {
      Webapi.Dom.EventTarget.addEventListener(
        eventName,
        handlerRef->React.Ref.current,
        element,
      );
    },
    () => {
      Webapi.Dom.EventTarget.removeEventListener(
        eventName,
        handlerRef->React.Ref.current,
        element,
      );
    },
  );
};

/**
 * Escape hatch: asDomElement
 *
 * Binds anything to a Dom.Element
 */
external asDomElement: 'a => Dom.element = "%identity";

/**
 * Escap hatch: nav
 *
 * Lazy document.location implmentation
 */
let nav: string => unit = {
  %bs.raw
  {|function(url) {
    var metaEl = document.head.querySelector('meta[name~=lunr-base-href]');
    if (metaEl) {
      url = metaEl.getAttribute('value') + url;
    }
    document.location.assign(url)
  }|};
};

/**
 * extracted from https://github.com/bevacqua/fuzzysearch/
 */
let fuzzysearch: (string, string) => bool = [%bs.raw
  {|function (needle, haystack) {
    var hlen = haystack.length;
    var nlen = needle.length;
    if (nlen > hlen) {
        return false;
    }
    if (nlen === hlen) {
        return needle === haystack;
    }
    outer: for (var i = 0, j = 0; i < nlen; i++) {
        var nch = needle.charCodeAt(i);
        while (j < hlen) {
        if (haystack.charCodeAt(j++) === nch) {
            continue outer;
        }
        }
        return false;
    }
    return true;
  }|}
];