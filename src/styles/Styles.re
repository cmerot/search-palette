open Css;

let palette =
  style([
    position(`fixed),
    top(zero),
    width(`percent(100.0)),
    display(`flex),
    alignItems(`center),
    justifyContent(`center),
    selector(
      " > div",
      [
        flexGrow(0.0),
        flexShrink(0),
        flexBasis(px(500)),
        unsafe(
          "boxShadow",
          "0 8px 10px 0 rgba(0, 0, 0, 0.14), 0 3px 14px 2px rgba(0, 0, 0, 0.12), 0 5px 5px -3px rgba(0, 0, 0, 0.2)",
        ),
        selector(" > div > ul", [maxHeight(px(200))]),
      ],
    ),
  ]);

let search =
  style([
    backgroundColor(hex("f3f3f3")),
    paddingTop(px(1)),
    paddingBottom(px(1)),
  ]);
let inputWrapper =
  style([
    padding(px(4)),
    margin(px(4)),
    border(px(1), `solid, hex("99cbe9")),
    backgroundColor(white),
  ]);
let input =
  style([
    display(`block),
    width(`percent(100.0)),
    padding(zero),
    margin(zero),
    border(px(0), `none, `transparent),
    outline(px(0), `none, `transparent),
    backgroundColor(`transparent),
    color(black),
  ]);
let results =
  style([
    overflow(`auto),
    listStyleType(`none),
    padding(zero),
    margin(zero),
  ]);

let result = isSelected =>
  style([
    backgroundColor(isSelected ? hex("ccecfe") : `transparent),
    fontSize(px(14)),
    selector("p", [marginTop(em(0.3)), marginBottom(em(0.3))]),
    padding(px(5)),
    cursor(`pointer),
    selector(
      "&:hover",
      [backgroundColor(isSelected ? hex("ccecfe") : hex("e7e7e7"))],
    ),
  ]);
let title =
  style([
    fontWeight(`normal),
    fontSize(px(14)),
    margin(zero),
    color(hex("616161")),
  ]);
let url = style([color(green), fontSize(px(12))]);
let snippet = style([color(rgb(84, 84, 84)), fontSize(px(12))]);