type tObjectDocument = {
  .
  "id": string,
  "title": string,
  "body": string,
};
external convertDocument: 'a => tObjectDocument = "%identity";

type item = Lunr.item;
type dataSource = Lunr.index;
let placeholder = "Search content...";

let getDocumentOfItem = (dataSource: dataSource, item: item) => {
  dataSource
  ->Lunr.documentStoreGet
  ->Lunr.docsGet
  ->Js.Dict.unsafeGet(item->Lunr.refGet)
  ->convertDocument;
};

/** search for value into dataSource, return items */
let search = (dataSource: dataSource, value: string): array(item) => {
  dataSource->Lunr.search(value, {"expand": true, "bool": "AND"});
};

/** transform a item into a React.element */
let renderItem = (dataSource: dataSource, item: item): React.element => {
  let doc = getDocumentOfItem(dataSource, item);
  <div>
    <h3 className=Styles.title> {React.string(doc##title)} </h3>
    <p className=Styles.url> {React.string(doc##id)} </p>
    <p className=Styles.snippet>
      {React.string(
         String.length(doc##body) < 240
           ? doc##body : String.sub(doc##body, 0, 240),
       )}
    </p>
  </div>;
};

let onClick = item => Utils.nav(item->Lunr.refGet);