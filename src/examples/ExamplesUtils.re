let getLunrDataSource = serializedIndex => Lunr.load(serializedIndex);

let getFuzzyDataSource = serializedIndex =>
  Set.String.fromArray(
    Array.map(
      Lunr.load(serializedIndex)
      ->Lunr.documentStoreGet
      ->Lunr.docsGet
      ->Js.Dict.keys,
      key =>
      Js.String.replaceByRe([%re "/#.*$/"], "", key)
    ),
  );