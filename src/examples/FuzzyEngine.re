type item = string;
type dataSource = Belt.Set.String.t;
let placeholder = "Search files...";

/** search for value into dataSource, return items */
let search = (dataSource: dataSource, value: string): array(item) => {
  Js.log("Seaching fuzzy");
  switch (value) {
  | "" => Set.String.toArray(dataSource)
  | _ =>
    Set.String.keep(dataSource, item => Utils.fuzzysearch(value, item))
    ->Set.String.toArray
  };
};

/** transform a item into a React.element */
let renderItem = (_, item: item): React.element => {
  <p> {React.string(item)} </p>;
};

let onClick = item => Utils.nav(item);