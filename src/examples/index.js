import * as fuzzyEngine from "./FuzzyEngine.bs";
import * as lunrEngine from "./LunrEngine.bs";
export { fuzzyEngine, lunrEngine };
export { getLunrDataSource, getFuzzyDataSource } from "./ExamplesUtils.bs";
