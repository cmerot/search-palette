type t;

type document;
[@bs.deriving abstract]
type documentStore = {docs: Js.Dict.t(document)};
[@bs.deriving abstract]
type index = {documentStore};

[@bs.deriving abstract]
type item = {
  ref: string,
  score: float,
};

[@bs.module] external lunr: t = "elasticlunr";
[@bs.send] [@bs.scope "Index"]
external load_: (t, Js.t({..})) => index = "load";
[@bs.send] external search: (index, string, Js.t({..})) => array(item) = "";

let load = serializedIndex => {
  lunr->load_(serializedIndex);
};