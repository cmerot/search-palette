[@bs.deriving abstract]
type item = {
  key: string,
  component: ReasonReact.reactElement,
};

type config = Js.Array.t(item);

[@react.component]
let make = (~config: config) => {
  let (activeItem, setActiveItem) = React.useState(() => None);
  let containerRef = React.useRef(Js.Nullable.null);

  let listConfig = List.fromArray(config);

  let document = Webapi.Dom.Document.asEventTarget(Webapi.Dom.document);

  let (registerShow, unregisterShow) =
    Utils.useEventListener(
      document,
      "keydown",
      e => {
        let event = e->Utils.eventToKeyboardEvent;
        let key = Webapi.Dom.KeyboardEvent.key(event);
        if (event->Webapi.Dom.KeyboardEvent.altKey && key != "Alt") {
          setActiveItem(_ => {
            let item = List.getBy(listConfig, item => key == item->keyGet);
            switch (item) {
            | None => ()
            | Some(_) => event->Webapi.Dom.KeyboardEvent.preventDefault
            };
            item;
          });
        };
      },
    );

  let (registerHide, unregisterHide) =
    Utils.useEventListener(
      document,
      "keydown",
      e => {
        let event = e->Utils.eventToKeyboardEvent;
        let key = Webapi.Dom.KeyboardEvent.key(event);
        if (key == "Escape") {
          setActiveItem(_ => None);
        };
      },
    );

  let (registerOutsideClick, unregisterOutsideClick) =
    Utils.useEventListener(document, "mousedown", e =>
      containerRef
      ->React.Ref.current
      ->Js.Nullable.toOption
      ->Option.map(searchElement => {
          let clickedElement = e->Webapi.Dom.Event.target->Utils.asDomElement;
          // If the click is outside the search box, we hide it
          if (!(searchElement |> Webapi.Dom.Element.contains(clickedElement))) {
            setActiveItem(_ => None);
          };
          None;
        })
      ->ignore
    );

  switch (activeItem) {
  | None =>
    unregisterHide();
    unregisterOutsideClick();
    registerShow();
  | _ =>
    unregisterShow();
    registerHide();
    registerOutsideClick();
  };

  <div ref={containerRef->ReactDOMRe.Ref.domRef}>
    {switch (activeItem) {
     | None => <span />
     | Some(item) => item->componentGet
     }}
  </div>;
};