[@react.component]
let make = (~dataSource, ~placeholder, ~search, ~renderItem, ~onClick) => {
  // Value for the <input>
  let (value, setValue) = React.useState(() => "");

  // Results of search(value)
  let (results, setResults) =
    React.useState(() => search(dataSource, value));

  // Index of the selected (highlighted) result
  let (selectedIndex, setSelectedIndex) = React.useState(() => None);

  // Those DOM refs are required to scroll the list
  // so we can see the selected result
  let listRef = React.useRef(Js.Nullable.null);
  let selectedItemRef = React.useRef(Js.Nullable.null);

  // Auto select first result each time the results change
  React.useEffect1(
    () => {
      setSelectedIndex(_ =>
        switch (results->Array.length) {
        | 0 => None
        | _ => Some(0)
        }
      );
      None;
    },
    [|results|],
  );

  // Scroll into view each time the selectedIndex changes
  React.useLayoutEffect1(
    () => {
      listRef
      ->React.Ref.current
      ->Js.Nullable.toOption
      ->Option.map(listEl =>
          selectedItemRef
          ->React.Ref.current
          ->Js.Nullable.toOption
          ->Option.map(childEl => Utils.scrollIntoView(listEl, childEl))
          ->ignore
        )
      ->ignore;

      None;
    },
    [|selectedIndex|],
  );

  // Update results based on the user input
  let onChange = event => {
    let value = ReactEvent.Form.target(event)##value;
    setValue(value);
    setResults(_ => search(dataSource, value));
  };

  // Keyboard navigation: up, down, enter will either change
  // the selected index, or trigger the onClick callback
  let onKeyDown = event => {
    ReactEvent.Keyboard.persist(event);
    let keyCode = ReactEvent.Keyboard.keyCode(event)->Utils.getKeyCode;
    switch (keyCode) {
    | ArrowUp =>
      setSelectedIndex(current =>
        switch (current) {
        | Some(index) =>
          switch (results[index - 1]) {
          | None => Some(index)
          | _ => Some(index - 1)
          }
        | None => Some(results->Array.length - 1)
        }
      )
    | ArrowDown =>
      setSelectedIndex(current =>
        switch (current) {
        | Some(index) =>
          switch (results[index + 1]) {
          | None => Some(index)
          | _ => Some(index + 1)
          }
        | None => Some(0)
        }
      )
    | Enter =>
      switch (selectedIndex) {
      | Some(i) => onClick(Array.getExn(results, i))
      | None => ()
      }
    | _ => ()
    };
  };

  <div onKeyDown className=Styles.search>
    <div className=Styles.inputWrapper>
      <input
        type_="text"
        autoFocus=true
        className=Styles.input
        placeholder
        value
        onChange
      />
    </div>
    {switch (results) {
     | [||] =>
       switch (value) {
       | "" => <div />
       | _ =>
         <ul tabIndex=(-1) className=Styles.results>
           <li className={Styles.result(true)}>
             <p className=Styles.snippet>
               {React.string("No result for ")}
               <em> {React.string(value)} </em>
             </p>
           </li>
         </ul>
       }
     | _ =>
       <ul
         tabIndex=(-1)
         className=Styles.results
         ref={listRef->ReactDOMRe.Ref.domRef}>
         {React.array(
            Array.mapWithIndex(
              results,
              (index, result) => {
                let isSelected =
                  switch (selectedIndex) {
                  | None => false
                  | Some(selectedIndex) => selectedIndex === index
                  };

                // We need to add a DOM ref only for the selected result,
                // otherwise components are the same
                isSelected
                  ? <li
                      className={Styles.result(isSelected)}
                      key={index->string_of_int}
                      onClick={_ => onClick(Array.getExn(results, index))}
                      ref={selectedItemRef->ReactDOMRe.Ref.domRef}>
                      {renderItem(dataSource, result)}
                    </li>
                  : <li
                      className={Styles.result(isSelected)}
                      key={index->string_of_int}
                      onClick={_ => onClick(Array.getExn(results, index))}>
                      {renderItem(dataSource, result)}
                    </li>;
              },
            ),
          )}
       </ul>
     }}
  </div>;
};