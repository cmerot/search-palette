// FuzzySearch component uses the Search ReasonML component
// and its `dataSource`, `search` and `renderItem` props also
// come from the Reason side.
// Because the search method requires a Reason specific structure
// for its dataSource, we also instanciate it on the Reason side.
// lunrIndex is made available in the global namespace by a `script` tag
// in the html file.
const fuzzyDataSource = searchPalette.examples.getFuzzyDataSource(lunrIndex);
const FuzzySearch = React.createElement(searchPalette.Search.make, {
  ...searchPalette.examples.fuzzyEngine,
  dataSource: fuzzyDataSource
});

// LunrSearch component
const lunrDataSource = searchPalette.examples.getLunrDataSource(lunrIndex);
const LunrSearch = React.createElement(searchPalette.Search.make, {
  ...searchPalette.examples.lunrEngine,
  dataSource: lunrDataSource
});

// another component, on the js side.
const MirrorSearch = React.createElement(searchPalette.Search.make, {
  dataSource: void 0,
  placeholder: "Mirror search...",
  search: (_, value) =>
    value
      ? [
          value,
          value
            .split("")
            .reverse()
            .join("")
        ]
      : [],
  renderItem: (_, item) => item,
  onClick: item => {
    alert(`Clicked ${item}`);
  }
});

// Get the app container and add a css class to it
const container = document.getElementById("app");
container.className = searchPalette.paletteClassName;

// Palette: a list of key/component pairs, also from the Reason
// side. Could be any component.
const Palette = React.createElement(searchPalette.Palette.make, {
  config: [
    { key: "c", component: LunrSearch },
    { key: "f", component: FuzzySearch },
    { key: "m", component: MirrorSearch }
  ]
});

ReactDOM.render(Palette, container);
