import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import replace from 'rollup-plugin-replace'

const { PRODUCTION } = process.env

export default {
  input: 'src/index.js',
  output: {
    file: 'dist/index.iife.js',
    format: 'iife',
    name: 'searchPalette',
    globals: {
      'react': 'React',
      'react-dom': 'ReactDOM',
      'elasticlunr': 'elasticlunr',
      'emotion': 'emotion'
    }
  },
  plugins: [
    resolve(),
    replace({
      'process.env.NODE_ENV': JSON.stringify(
        PRODUCTION ? 'production' : 'development'
      )
    }),
    commonjs({
      namedExports: {
        'node_modules/react/index.js': ['createElement', 'useRef', 'useState', 'useLayoutEffect', 'useEffect'],
        'node_modules/react-dom/index.js': ['render'],
      }
    }),
  ],
  external: [
    'react', 'react-dom', 'elasticlunr', 'emotion'
  ]
};
