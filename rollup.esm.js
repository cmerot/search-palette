import resolve from "rollup-plugin-node-resolve";
import commonjs from "rollup-plugin-commonjs";
import replace from "rollup-plugin-replace";

const { PRODUCTION } = process.env;

export default {
  input: "src/index.js",
  output: {
    file: "dist/index.esm.js",
    format: "esm",
    globals: {
      react: "React",
      "react-dom": "ReactDOM",
      elasticlunr: "elasticlunr",
      emotion: "emotion"
    },
    preferConst: true
  },
  plugins: [
    resolve(),
    replace({
      "process.env.NODE_ENV": JSON.stringify(
        PRODUCTION ? "production" : "development"
      )
    }),
    commonjs({
      namedExports: {
        "node_modules/react/index.js": [
          "createElement",
          "useRef",
          "useState",
          "useLayoutEffect",
          "useEffect"
        ],
        "node_modules/react-dom/index.js": ["render", "hydrate"]
      }
    })
  ],
  external: ["react", "react-dom", "elasticlunr", "emotion"]
};
